
import * as React from 'react';
import {FunctionComponent} from 'react';

import classNames from 'classnames';
import {Styled} from '../../utils';


const styles = require('./Container.scss');

export interface ContainerProps extends Styled {

    fluid?: boolean;

}

export const Container: FunctionComponent<ContainerProps> = (props) => {

    const {children, style, className, fluid} = props;

    return (
        <div
            style={style}
            className={classNames({
                [styles.container]: true,
                [styles.containerFluid]: fluid,
                className
            })} >

            {children}

        </div>
    );
}