
import {Action, Dispatch} from '../types';

import {UPDATE_PERSON} from '../types/personTypes';
import {IPersonState} from '../IAppState';


export const updatePerson = (newPerson: IPersonState): Action => {

    return (dispatch: Dispatch) => {

        dispatch({
            type: UPDATE_PERSON,
            payload: newPerson
        });
    }
}
