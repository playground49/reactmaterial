
import {Action, Dispatch} from '../types';
import {CREATE_ROOM, JOIN_ROOM} from '../types/authTypes';

import {FormProps} from '../../components/pages/LoginPage/LoginForm';


export const createRoom = (form: FormProps): Action => {

    return (dispatch: Dispatch) => {

        dispatch({
            type: CREATE_ROOM,
            payload: {
                ...form,
                token: ''
            }
        });
    };
}
export const joinRoom = (form: FormProps): Action => {

    return (dispatch: Dispatch) => {

        dispatch({
            type: JOIN_ROOM,
            payload: {
                ...form,
                token: ''
            }
        });
    };
}
