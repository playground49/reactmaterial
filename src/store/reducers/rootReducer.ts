
import {combineReducers, Reducer} from 'redux';

import {IAppState} from '../IAppState';
import {authReducer} from './authReducer';
import {personReducer} from './personReducer';


export const rootReducer: Reducer<IAppState> = combineReducers({
    auth: authReducer,
    person: personReducer
});
