
import {Reducer} from 'redux';

import {IAuth} from '../IAppState';
import {initialState} from '../initialState';
import {CREATE_ROOM, JOIN_ROOM} from '../types/authTypes';


export const authReducer: Reducer<IAuth> = (state: IAuth = initialState.auth, {type, payload}) => {

    switch (type) {

        case CREATE_ROOM:
        case JOIN_ROOM:
            return {
                ...state,
                ...payload
            };

        default:
            return state;
    }
}
