
import {IAppState} from './IAppState';


const state: IAppState = {

    auth: {
        token: undefined,
        user: '',
        room: ''
    },

    person: {
        name: '',
        lastName: '',
        age: 0
    }

};


export const initialState = process.env.NODE_ENV === 'development' ? {
    ...state,
    auth: {
        ...state.auth,
        user: 'test_admin',
        room: 'Lacasitos',
        token: 'mec'
    },
    person: {
        ...state.person,
        name: 'Gerard',
        lastName: 'Albiol',
        age: 26
    }
} : state;
