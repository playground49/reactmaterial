


export interface IAppState {

    auth: IAuth;

    person: IPersonState;

}

export interface IAuth {

    token: any;

    user: string;

    room: string;

}

export interface IPersonState {

    name: string;

    lastName: string;

    age: number;

}
