
import * as React from 'react';
import {FunctionComponent} from 'react';
import {Switch, Route} from 'react-router-dom';

import {LoginPage, HomePage} from '../pages';

import {routes} from '../../config/routes';

import '../../styles/main.scss';
import {PrivateRoute} from './PrivateRoute';


export const Routes: FunctionComponent = () => {

    return (
        <Switch>
            <Route exact name="login" path={routes.login} component={LoginPage} />
            <PrivateRoute exact name="home" path={routes.home} component={HomePage} />
        </Switch>
    );
}
