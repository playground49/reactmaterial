
import * as React from 'react';
import {FunctionComponent} from 'react';
import {useSelector} from 'react-redux';
import {Route, Redirect} from 'react-router-dom';

import {routes} from '../../config/routes';
import {IAppState} from '../../store/IAppState';


export const PrivateRoute: FunctionComponent<any> = (props) => {

    const {component: Component, ...rest} = props;

    const {token} = useSelector((state: IAppState) => state.auth);

    // TODO: Use localStorage to set the token
    const isAuthenticated = !!token;

    return (
        <Route {...rest}
               render={(props) => {
                   return !isAuthenticated ? (
                       <Redirect to={routes.login} />
                   ):(
                       <Component {...props} />
                   )
               }} />
    );
};
