
import * as React from 'react';
import {FunctionComponent} from 'react';

import {createMuiTheme} from '@material-ui/core/styles';
import {teal} from '@material-ui/core/colors';
import {ThemeProvider} from '@material-ui/styles';


export const MuiThemeProvider: FunctionComponent = (props) => {

    const theme = createMuiTheme({
        palette: {
            primary: {
                main: '#706fd3',
                // main: '#11cb5f',
                contrastText: 'white'
            },
            secondary: {
                main: teal[500],
                contrastText: 'white'
            }
        },
        props: {
            MuiButton: {
                variant: 'contained',
                color: 'primary'
            }
        }
    });

    return (
        <ThemeProvider theme={theme} >

            {props.children}

        </ThemeProvider>
    );
};
