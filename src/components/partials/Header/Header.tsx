
import React from 'react';
import {FunctionComponent, useState} from 'react';

import classNames from 'classnames';
import {Theme} from './HeaderType';


const styles = require('./Header.scss');

interface HeaderProps {
    theme: Theme;
}

export const Header: FunctionComponent<HeaderProps> = (props) => {

    const [tabs, setTabs] = useState<string[]>([]);

    const addNewTab = (): void => {

        setTabs([
            ...tabs,
            `tab-${tabs.length + 1}`
        ]);
    }

    return (
        <div className={classNames(styles.headerWrapper, styles[props.theme])} >

            <div data-test="clickBrand" className={styles.brand} onClick={addNewTab} >
                Header
            </div>

            <div className={styles.tabs} >

                {tabs.map((tab, index) => (
                    <div data-test="item" key={index} className={styles.tab} >
                        {tab}
                    </div>
                ))}

            </div>

        </div>
    );
}
