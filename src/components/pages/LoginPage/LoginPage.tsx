
import * as React from 'react';
import {FunctionComponent, useState} from 'react';
import {useDispatch} from 'react-redux';

import {
    Typography,
    Accordion,
    AccordionSummary,
    AccordionDetails, Snackbar
} from '@material-ui/core';
import {Alert} from '@material-ui/lab';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import {Dispatch} from '../../../store/types/redux';
import {createRoom, joinRoom} from '../../../store/actions/authActions';

import {Container} from '../../../__lib__/react-components/layout';

import {FormProps, LoginForm} from './LoginForm';


const styles = require('./LoginPage.scss');

export const LoginPage: FunctionComponent = () => {

    const dispatch: Dispatch = useDispatch();
    const createRoomDispatch = (state: FormProps) => dispatch(createRoom(state));
    const joinRoomDispatch = (state: FormProps) => dispatch(joinRoom(state));

    const [toggleAccordion, setToggleAccordion] = useState<boolean>(true);
    const [snackbarError, setSnackError] = useState<boolean>(false);


    const closeSnackbar = (): void => {
        setSnackError(false);
    }

    const checkForm = (form: FormProps, cd: (state: FormProps) => Promise<void> | void) => {

        if(!!form.user && !!form.room) {
            cd(form);
        } else {
            setSnackError(true);
        }
    }

    const onCreateClick = (form: FormProps) => {
        checkForm(form, createRoomDispatch);
    }

    const onJoinClick = (form: FormProps) => {
        checkForm(form, joinRoomDispatch);
    }

    return (
        <Container>

            <div className={styles.wrapper} >

                <Typography variant="h2"
                            component="h2"
                            className={styles.title} >
                    Cards Against Humanity
                </Typography>

                <Accordion expanded={toggleAccordion} onClick={() => setToggleAccordion(!toggleAccordion)} >
                    <AccordionSummary expandIcon={<ExpandMoreIcon />}
                                      aria-controls="panel1a-content"
                                      id="panel1a-header" >
                        <Typography>Crear una partida</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <LoginForm onSubmit={onCreateClick} />
                    </AccordionDetails>
                </Accordion>

                <Accordion expanded={!toggleAccordion} onClick={() => setToggleAccordion(!toggleAccordion)} >
                    <AccordionSummary expandIcon={<ExpandMoreIcon />}
                                      aria-controls="panel2a-content"
                                      id="panel2a-header" >
                        <Typography>Unirse a una partida</Typography>
                    </AccordionSummary>
                    <AccordionDetails>
                        <LoginForm onSubmit={onJoinClick} />
                    </AccordionDetails>
                </Accordion>

            </div>

            <Snackbar open={snackbarError}
                      autoHideDuration={5000}
                      onClose={closeSnackbar} >

                <Alert onClose={closeSnackbar} severity="error" >
                    This is a success message!
                </Alert>

            </Snackbar>

        </Container>
    );
}
