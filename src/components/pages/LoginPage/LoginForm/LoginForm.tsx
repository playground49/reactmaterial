
import * as React from 'react';
import {ChangeEvent, FunctionComponent, useState} from 'react';

import {
    Button,
    Paper,
    TextField,
    Snackbar,
} from '@material-ui/core';
import {Alert} from '@material-ui/lab';


const styles = require('./LoginForm.scss');

export interface FormProps {
    user: string;
    room: string;
}

interface LoginFormProps {
    onSubmit(form: FormProps): void;
}

export const LoginForm: FunctionComponent<LoginFormProps> = (props) => {

    const {onSubmit} = props;

    const [form, setForm] = useState<FormProps>({user: '', room: ''});

    const onInputChange = (event: ChangeEvent<HTMLInputElement>) => {
        setForm({
            ...form,
            [event.currentTarget.name]: event.currentTarget.value
        });
    }

    return (
        <Paper className={styles.content} elevation={2} >

            <TextField name="user"
                       label="Nombre de usuario"
                       onChange={onInputChange} />

            <TextField name="room"
                       label="Nombre de la sala"
                       onChange={onInputChange} />

            <Button onClick={() => onSubmit(form)} >
                Empezar
            </Button>

        </Paper>
    );
};
