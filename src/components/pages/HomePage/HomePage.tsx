
import * as React from 'react';
import {FunctionComponent, useState} from 'react';
import {useSelector} from 'react-redux';

import {
    Typography,
    Button
} from '@material-ui/core';

import {IAppState} from '../../../store/IAppState';

import {Container} from '../../../__lib__/react-components/layout';


const styles = require('./HomePage.scss');

export const HomePage: FunctionComponent = () => {

    const [readyState, setReadyState] = useState<boolean>(false);

    const {user, room} = useSelector((state: IAppState) =>state.auth);


    const onReadyClick = (): void => {
        setReadyState(true);
    }

    const onPlayClick = (): void => {
        console.log('PLAY');
    }

    return (
        <Container>

            <div className={styles.wrapper} >

                <div className={styles.titleWrapper} >
                    <Typography variant="h2" component="h2" >
                        Sala de espera
                    </Typography>

                    <Typography variant="h4" component="p" >
                        {room}
                    </Typography>
                </div>

                <div className={styles.playersWrapper} >
                    <Typography variant="h6" component="div" >
                        Lista de jugadores:
                    </Typography>

                    <ul>
                        <li>{user}</li>
                        <li>1</li>
                        <li>1</li>
                        <li>1</li>
                    </ul>
                </div>

                <div className={styles.buttonWrapper} >
                    <Button onClick={onReadyClick} disabled={readyState} >
                        Estoy listo!
                    </Button>

                    {readyState && (
                        <Typography variant="body1" component="div" className={styles.waitingText} >
                            Esperando al resto de jugadores...
                        </Typography>
                    )}
                </div>

                <div className={styles.adminBtn} >
                    <Typography variant="body1" component="div" >
                        Boton solo visible para el creador de la sala. Cuando todos los jugadores esten preparados, clica en empezar partida.
                    </Typography>

                    <Button color="primary" onClick={onPlayClick} >
                        Empezar partida
                    </Button>
                </div>

            </div>

        </Container>
    );
}
