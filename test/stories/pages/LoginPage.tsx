
import * as React from 'react';
import {Provider} from 'react-redux';
import {storiesOf} from '@storybook/react';

import {MuiThemeProvider} from '../../../src/components/MuiThemeProvider';
import {LoginPage} from '../../../src/components/pages';

import {ComponentWrapper, createStoryStore} from '../utils';
import {IAppState} from '../../../src/store/IAppState';


const state: IAppState = {

    auth: {
        token: '',
        user: '',
        room: ''
    },

    person: {
        name: 'Gerard',
        lastName: 'Foo',
        age: 10
    }

};

const store = createStoryStore(state);

storiesOf('pages/Login', module)
    .addDecorator(story => (
        <MuiThemeProvider>
            <Provider store={store} >
                <ComponentWrapper style={{display: 'block'}} >
                    {story()}
                </ComponentWrapper>
            </Provider>
        </MuiThemeProvider>
    ))
    .add('default', () => (
        <LoginPage />
    ));
