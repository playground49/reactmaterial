
import * as React from 'react';

import {configure, shallow} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';

import {Header} from '../../../../src/components/partials';


configure({adapter: new Adapter()});

describe('Header', () => {

    test('Create new tab', () => {

        // Mock
        const wrapper = shallow(<Header theme="dark" />);

        // Simulation
        wrapper.find('div[data-test="clickBrand"]').simulate('click');

        const items = wrapper.find('div[data-test="item"]');

        // Assertion
        expect(items.length).toBe(1);
    });

});
