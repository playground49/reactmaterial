
import {Configuration, HotModuleReplacementPlugin} from 'webpack';
import FaviconsWebpackPlugin from 'favicons-webpack-plugin';
import path from 'path';

import mainConfig from '../webpack.config';
import {styleRule} from '../rules';


const config: Configuration = {
    
    ...mainConfig,

    entry: path.resolve(__dirname, '..', '..', 'src', 'index.dev.tsx'),

    mode: 'development',

    devtool: 'source-map',

    devServer: {
        contentBase: path.resolve(__dirname, '..', '..', 'public'),
        port: 3000,
        hot: true
    },

    module: {
        ...mainConfig.module,
        rules: [
            ...mainConfig.module!.rules,
            styleRule('development'),
            {
                enforce: 'pre',
                test: /\.js$/,
                loader: 'source-map-loader'
            }
        ]
    },

    plugins: [
        ...mainConfig!.plugins,
        // new FaviconsWebpackPlugin(path.resolve(__dirname, '..', '..', 'public', 'favicon.ico')),
        new HotModuleReplacementPlugin()
    ]

};


export default config;
