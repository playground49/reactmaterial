
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import path from 'path';


export function styleRule(environment: string): object {
    
    const localIdentName = environment !== 'production' ? '[name]-[local]' : '[hash:base64:5]';

    return {
        test: /\.scss$/,
        use: [
            MiniCssExtractPlugin.loader,
            {
                loader: 'css-loader',
                options: {
                    modules: {
                        localIdentName
                    },
                    importLoaders: 2,
                    localsConvention: 'camelCase',
                    sourceMap: true
                }
            },
            'sass-loader',
            {
                loader: 'sass-resources-loader',
                options: {
                    resources: path.resolve(__dirname, '..', '..', 'src', 'styles', '_variables.scss')
                }
            }
        ]
    };
}
